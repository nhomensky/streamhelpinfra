
CREATE OR REPLACE FUNCTION new_partition_creator() RETURNS trigger AS
  $BODY$
    DECLARE
partition_date TEXT;
      partition TEXT;
      partition_day int;
      startdate date;
      enddate date;
BEGIN
      partition_day := to_char(NEW.logdate,'DD');
      partition_date := to_char(NEW.logdate,'YYYY_MM');
      startdate := to_char(NEW.logdate,'YYYY-MM-DD');
      enddate := to_char(NEW.logdate + INTERVAL '1 day','YYYY-MM-DD');
      partition := TG_TABLE_NAME || '_' || 'part_' || startdate;


      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
EXECUTE 'CREATE TABLE ' || partition || 'partition of ' || TG_TABLE_NAME || 'for values from (''' || startdate || ''') to (''' || enddate || ''');';
RETURN NULL;
end if;
END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;