CREATE OR REPLACE FUNCTION show_create_table(
    in_schema_name varchar,
    in_table_name varchar,
    column_key text
)
    RETURNS text
    LANGUAGE plpgsql VOLATILE
AS
$$
DECLARE
    -- the ddl we're building
    v_table_ddl text;

    -- data about the target table
    v_table_oid int;

    -- records for looping
    v_column_record record;
    v_constraint_record record;
    v_index_record record;
    dt_type varchar;
begin
    --out_table := 'transaction';
    -- grab the oid of the table; https://www.postgresql.org/docs/8.3/catalog-pg-class.html
    SELECT c.oid INTO v_table_oid
    FROM pg_catalog.pg_class c
             LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
    WHERE 1=1
      AND c.relkind = 'r' -- r = ordinary table; https://www.postgresql.org/docs/9.3/catalog-pg-class.html
      AND c.relname = in_table_name -- the table name
      AND n.nspname = in_schema_name; -- the schema

-- throw an error if table was not found
    IF (v_table_oid IS NULL) then
        SELECT c.oid INTO v_table_oid
        FROM pg_catalog.pg_class c
                 LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE 1=1
          AND c.relkind = 'p' -- r = ordinary table; https://www.postgresql.org/docs/9.3/catalog-pg-class.html
          AND c.relname = in_table_name -- the table name
          AND n.nspname = in_schema_name;
        if (v_table_oid IS not null) then
            RAISE EXCEPTION 'already partitioned and table exists';
        end if;
        RAISE EXCEPTION 'table does not exist';

    END IF;

    -- start the create definition
    v_table_ddl := 'CREATE TABLE ' || in_schema_name || '.' || in_table_name || ' (' || E'\n';

    -- define all of the columns in the table; https://stackoverflow.com/a/8153081/3068233
    FOR v_column_record IN
        SELECT
            c.column_name,
            c.data_type,
            c.character_maximum_length,
            c.is_nullable,
            c.column_default
        FROM information_schema.columns c
        WHERE (table_schema, table_name) = (in_schema_name, in_table_name)
        ORDER BY ordinal_position
        loop
            dt_type := (select check_data_type(in_schema_name,in_table_name,v_column_record.column_name));
            v_table_ddl := v_table_ddl || '  ' -- note: two char spacer to start, to indent the column
                               || v_column_record.column_name || ' '
                               || dt_type || CASE WHEN v_column_record.character_maximum_length IS NOT NULL THEN ('(' || v_column_record.character_maximum_length || ')') ELSE '' END || ' '
                               || CASE WHEN v_column_record.is_nullable = 'NO' THEN 'NOT NULL' ELSE 'NULL' END
                               || CASE WHEN v_column_record.column_default IS NOT null THEN (' DEFAULT ' || v_column_record.column_default) ELSE '' END
                               || ',' || E'\n';
        END LOOP;
    ------------
    -------------
    --------------
    -------------
    -------------
    FOR v_constraint_record in

        SELECT
            con.conname as constraint_name,
            con.contype as constraint_type,
            CASE
                WHEN con.contype = 'p' THEN 1 -- primary key constraint
                WHEN con.contype = 'u' THEN 2 -- unique constraint
                WHEN con.contype = 'f' THEN 3 -- foreign key constraint
                WHEN con.contype = 'c' THEN 4
                ELSE 5
                END as type_rank,
            case
                when pg_get_constraintdef(con.oid) like 'PRIMARY KEY%' then regexp_replace(pg_get_constraintdef(con.oid),'(PRIMARY KEY \()(.*)(\))','PRIMARY KEY (\2,' || column_key || ')')
                when pg_get_constraintdef(con.oid) like 'UNIQUE%' then regexp_replace(pg_get_constraintdef(con.oid),'(UNIQUE \()(.*)(\))','UNIQUE (\2,' || column_key || ')')
                else pg_get_constraintdef(con.oid)
                end
                        as constraint_definition
        FROM pg_catalog.pg_constraint con
                 JOIN pg_catalog.pg_class rel ON rel.oid = con.conrelid
                 JOIN pg_catalog.pg_namespace nsp ON nsp.oid = connamespace
        WHERE nsp.nspname = in_schema_name
          AND rel.relname = in_table_name
        ORDER BY type_rank
        loop
            v_constraint_record.constraint_name := (select replace(v_constraint_record.constraint_name,v_constraint_record.constraint_name,in_table_name || '_' || v_constraint_record.constraint_name));
            v_table_ddl := v_table_ddl || '  CONSTRAINT' || ' '
                               || v_constraint_record.constraint_name || ' '
                               || v_constraint_record.constraint_definition
                               || ',' || E'\n';
        END LOOP;
    v_table_ddl = substr(v_table_ddl, 0, length(v_table_ddl) - 1) || E'\n';

    v_table_ddl := v_table_ddl || ') ';

    RETURN v_table_ddl;
END;
$$;

CREATE OR REPLACE FUNCTION check_data_type(  -- из указанной таблицы собрать ограничения и индексы
    schema_name varchar,
    table_name2 varchar,
    column_name2 varchar
)
    RETURNS text
    LANGUAGE plpgsql VOLATILE
AS
$$
declare
    dt RECORD;
    data_type2 text;
begin
    data_type2 := '';
    FOR dt in
        SELECT
            data_type,numeric_precision,numeric_scale,datetime_precision
        FROM information_schema.columns c
        WHERE (table_schema, table_name) = (schema_name, table_name2) and c.column_name = column_name2
        ORDER BY ordinal_position
        loop
            if dt.data_type = 'timestamp without time zone' then
                if dt.data_type = 'timestamp without time zone' then
                    data_type2 := 'timestamp(' || dt.datetime_precision || ')';
                elseif dt.data_type = 'timestamp with time zone' then
                    data_type2 := 'timestamptz(' || dt.datetime_precision || ')';
                end if;
                raise notice 'datatype timestamp %',data_type2;


            elseif dt.data_type = 'numeric' then
                if dt.numeric_scale is null then
                    data_type2 := dt.data_type || '(' || dt.numeric_precision || ')';
                elseif (dt.numeric_scale is null and dt.numeric_precision is null) then
                    data_type2 := dt.data_type;
                else
                    data_type2 := dt.data_type || '(' || dt.numeric_precision || ',' || dt.numeric_scale || ')';
                end if ;
            else
                data_type2 := dt.data_type;
            end if;
        end LOOP;
    return data_type2;
END;
$$;


CREATE OR REPLACE FUNCTION show_alter_table(  -- из указанной таблицы собрать ограничения и индексы
    in_schema_name varchar,
    in_table_name varchar,
    out_table varchar
)
    RETURNS text
    LANGUAGE plpgsql VOLATILE
AS
$$
DECLARE
    -- the ddl we're building
    v_table_ddl text;

    -- data about the target table
    v_table_oid int; -- cтарая таблица которую мы будем изменять

    -- records for looping
    v_column_record record; -- переменная для колонок
    v_constraint_record record; -- переменная для записей
    v_index_record record; -- переменная для индексов
BEGIN
    -- grab the oid of the table; https://www.postgresql.org/docs/8.3/catalog-pg-class.html
    SELECT c.oid INTO v_table_oid -- выбрать индефикаторы обьекта
    FROM pg_catalog.pg_class c
             LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace -- обьединение таблиц
    WHERE 1=1
      AND c.relkind = 'r'        -- r = ordinary table; https://www.postgresql.org/docs/9.3/catalog-pg-class.html
      AND c.relname = in_table_name -- the table name
      AND n.nspname = in_schema_name; -- the schema

-- throw an error if table was not found
    IF (v_table_oid IS NULL) THEN
        RAISE EXCEPTION 'table does not exist';
    END IF;

    v_table_ddl := '';

    FOR v_index_record IN
        SELECT indexdef
        FROM pg_indexes
        WHERE (schemaname, tablename) = (in_schema_name, in_table_name)
          and indexdef not like 'CREATE UNIQUE INDEX%'
        LOOP
            v_table_ddl := v_table_ddl
                               || v_index_record.indexdef
                               || ';' || E'\n';
            v_table_ddl := (select regexp_replace(v_table_ddl,in_table_name,'q'));

            v_table_ddl := (select regexp_replace(v_table_ddl,'(CREATE INDEX )(\w+)( ON)','\1 ' || out_table || '_\2 ON'));
            v_table_ddl := (select regexp_replace(v_table_ddl,'(ON )(' || in_schema_name || '.\w+)( USING)','\1 ' || in_schema_name || '.' || out_table || ' USING'));
            v_table_ddl := (select regexp_replace(v_table_ddl,in_table_name,'z'));

        END LOOP;
    RETURN v_table_ddl;
END;
$$;

--
--CREATE OR REPLACE function  new_partition_creator(startdate date, table_name text) RETURNS integer as
--$BODY$
--DECLARE
--    partition1 text;
--    enddate date;
--    startpart varchar;
--    alter_part text;
--    table_old text;
--BEGIN
--    enddate := startdate + INTERVAL '1 day';
--    startpart := to_char(startdate,'YYYY_MM_DD');
--    partition1 := table_name || '_' || 'part_' || startpart;
--    IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition1) THEN
--        RAISE NOTICE 'A partition has been created %',startdate;
--        EXECUTE 'CREATE TABLE ' || partition1 || ' partition of ' || table_name || ' for values from ( ''' || startdate || ''' ) to ( ''' || enddate || ''' );';
--        RETURN 1;
--    else
--        return 222;
--    end if;
--END;
--$BODY$
--    LANGUAGE plpgsql VOLATILE
--                     COST 100;

-- Эта функция создает новую таблицу с таким же именем как секционированную и
-- цепляет старую как секцию для данных до сегодняшнего дня включительно
-- Так же создается секция на завтрашний день и дефолтная на всякий случай

CREATE OR REPLACE function  partitioninig_on(table_name text, column_key text) RETURNS integer as
$BODY$
DECLARE
    tm varchar;
    atm varchar;
    tm_part varchar;
    tmdate date;
    table_ddl text;
    alter_part text;
    alter_def text;
    part_name varchar;
    table_old text;
    table_def text;
    alter_main text;
    table_old_pkey text;
    drop_old_con text;
    role_name text;
begin
--	tm := to_char(now() + INTERVAL '1 day' ,'YYYY-MM-DD');        
--  atm := to_char(now() + INTERVAL '2 day','YYYY-MM-DD');
    tm := to_char(date_trunc('month',now() + INTERVAL '1 month') ,'YYYY-MM-DD');        
    atm := to_char(date_trunc('month',now() + INTERVAL '2 month'),'YYYY-MM-DD');
    tmdate := to_date(tm,'YYYY-MM-DD');
    tm_part := to_char(tmdate,'YYYY_MM');
    table_ddl:= (SELECT show_create_table('public',table_name,column_key));
    table_ddl := substr(table_ddl, 0, length(table_ddl) - 1) || E'\n';
    table_ddl := table_ddl || E'\n' || ')' || E'\n';
    role_name:= (SELECT current_database());
    table_ddl := table_ddl || 'PARTITION BY RANGE (' || column_key || '); GRANT ALL PRIVILEGES ON TABLE ' || table_name || ' TO "' || role_name || '";' ;
    table_old := table_name || '_old';
    table_def := table_name || '_default';
    table_old_pkey := (SELECT
                           con.conname as constraint_name
                       FROM pg_catalog.pg_constraint con
                                JOIN pg_catalog.pg_class rel ON rel.oid = con.conrelid
                                JOIN pg_catalog.pg_namespace nsp ON nsp.oid = connamespace
                       WHERE nsp.nspname = 'public'--in_schema_name
                         AND rel.relname = table_name
                         and pg_get_constraintdef(con.oid) like 'PRIMARY KEY%');

    RAISE NOTICE 'DROP_key: %',table_old_pkey;
    drop_old_con := 'alter table ' || table_old || ' drop constraint ' || table_old_pkey || ';';
    RAISE NOTICE 'DROP_constrain: %',drop_old_con;
    EXECUTE 'alter table ' || table_name || ' rename to ' || table_name || '_old;';
    RAISE NOTICE 'QUERY_table_ddal: %',table_ddl;
    execute '' || table_ddl || '';
    EXECUTE 'create  table ' || table_name || '_part_' || tm_part || ' partition of ' || table_name || ' for values from (''' || tm || ''') to (''' || atm || ''');';
    execute 'create  table ' || table_def || ' partition of ' || table_name || ' default;';
    if (table_old_pkey is not NULL) then
        execute '' || drop_old_con || '';
    end if;
    EXECUTE 'ALTER TABLE ' || table_name || ' ATTACH PARTITION ' || table_old || ' FOR VALUES from (''2000-01-01'') to (''' || tm || ''');';
    alter_main := (SELECT show_alter_table('public',table_old,table_name));
    execute '' || alter_main || '';
    RETURN 1;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE
                     COST 100;



-- test

CREATE OR REPLACE function  new_partition_creator(startdate date, table_name text) RETURNS integer as
$BODY$
DECLARE
    partition1 text;
    enddate date;
    startpart varchar;
    alter_part text;
    table_old text;
BEGIN
	startdate := date_trunc('month',startdate + INTERVAL '1 month');
    enddate := startdate + INTERVAL '1 month';     --tommorow + 1 moun
    startpart := to_char(startdate,'YYYY_MM');
    partition1 := table_name || '_' || 'part_' || startpart;
    IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition1) THEN
        RAISE NOTICE 'A partition has been created %',startdate;
        RAISE NOTICE  'A last day of month %',enddate;
        RAISE NOTICE  'Table name %',partition1;
        EXECUTE 'CREATE TABLE ' || partition1 || ' partition of ' || table_name || ' for values from ( ''' || startdate || ''' ) to ( ''' || enddate || ''' );';
        RETURN 1;
    else
        return 222;
    end if;
    --return 1;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE
                     COST 100;
                    
--select new_partition_creator('2022-02-02','transaction');
select partitioninig_on('transaction', 'created_date');
                    