<?php
//// Создаем архивную таблицу со старыми данными
sleep(5);
$dbconn = pg_connect("host=infra-postgres port=5432 dbname=postgres user=postgres");
var_dump($dbconn);
$query = pg_query($dbconn, "truncate table log");
if ($query) {
    echo "Added partitioned table!\n";
}
$base_create = pg_query($dbconn,"CREATE TABLE if not exists public.log (
	date date NOT NULL,
	state int default 1
)
");
$i=1;
while ($i<10) {
    $query = pg_query($dbconn, "INSERT  INTO log(date) VALUES ((now())-'$i day'::interval);");
    if ($query) {
        echo "Record Successfully Added!\n";
    }
    $i++;
}

//Теперь нужно создать секционированную таблицу для вставки новых данных
$query = pg_query($dbconn, "CREATE TABLE if not exists new_log (
date date NOT NULL,
state int default 1
) PARTITION BY RANGE (date)");
if ($query) {
    echo "Added partitioned table!\n";
}
//Добавляем секцию по умолчанию для еривых данных
$query = pg_query($dbconn, "CREATE TABLE if not exists new_log_def PARTITION OF new_log
    default;");
if ($query) {
    echo "Added default partition\n";
}
///Создаем триггер для создания партиции на вставку



?>